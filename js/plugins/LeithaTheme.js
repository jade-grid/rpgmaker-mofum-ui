/*:
 * @plugindesc 依赖Mofum UI插件
 *
 * @author 玉米
 * @param theme
 * @desc 主题
 * @param autoEnable
 * @desc 自动启动
 */
(function() {

	function checkLib() {
		try {
			if ($MofumUI) {
				return true;
			}
		} catch (e) {
			return false;
		}
		return false;
	};


	function init() {
		if (!checkLib()) {
			init();
			return;
		}
	}

	init();

	/**
	 * 重写启动页
	 */
	(function() {
		var _Scene_Boot_start = Scene_Boot.prototype.start;
		Scene_Boot.prototype.start = function() {
			$MofumUI.goto("main_menu");
			if ($MofumUI.currentFrame()) {
				$MofumUI.currentFrame().show();
			}

		};

		Graphics._modifyExistingElements = function() {
			var elements = document.getElementsByTagName('*');
			for (var i = 0; i < elements.length; i++) {
				if (elements[i].style.zIndex > 0) {
					// elements[i].style.zIndex = 0;
				}
			}
		};

		Scene_GameEnd.prototype.commandToTitle = function() {
			this.fadeOutAll();
			SceneManager.goto(Scene_Boot);
		};

		Scene_Map.prototype.isMenuCalled = function() {
			$MofumUI.currentFrame().show();
			return Input.isTriggered('menu') || TouchInput.isCancelled();
		};

		Scene_Map.prototype.callMenu = function() {
			$MofumUI.currentFrame().hide();
			SoundManager.playOk();
			SceneManager.push(Scene_Menu);
			Window_MenuCommand.initCommandPosition();
			$gameTemp.clearDestination();
			this._mapNameWindow.hide();
			this._waitCount = 2;
		};

		Scene_Map.prototype.createMapNameWindow = function() {
			this._mapNameWindow = {
				"open": function() {},
				"close": function() {},
				"hide": function() {}
			}
		};

	})();

	$MofumUI.start();

	//导入UI类 import Classes;
	var BaseImage = $MofumUI.loadClassByName("BaseImage");
	var BaseButton = $MofumUI.loadClassByName("BaseButton");
	var Frame = $MofumUI.loadClassByName("Frame");
	var Panel = $MofumUI.loadClassByName("Panel");

	/**
	 * 主菜单界面
	 */
	$MofumUI.UI("main_menu", function(modelName, cssFile) {
		var frame = new Frame(modelName, cssFile);
		var borderLayout = frame.layout;
		var southPanel = borderLayout.getSouth();
		southPanel.setHeight(100);


		var image = new BaseImage("img/titles1/Book.png");
		image.setWidth(window.innerWidth);
		image.setHeight(window.innerHeight);
		//窗口变化自适应
		image.on("windowResize", function(e) {
			image.setWidth(e.detail.width);
			image.setHeight(e.detail.height);
		}.bind(image));
		//收到消息事件，例如mqtt消息
		image.on("message", function(e) {
			console.log(e.detail.message.toString());
		}.bind(image))
		borderLayout.getBackground().appendChild(image);
		
		//开始游戏按钮
		var startButton = new BaseButton("开始游戏");
		startButton.setStyleClass("base-button button-7")
		startButton.on("mixedClick", function() {
			$MofumUI.currentFrame().hide();
			$MofumUI.stopTimer();
			SoundManager.playOk();
			DataManager.setupNewGame();
			SceneManager._scene.fadeOutAll();
			SceneManager.goto(Scene_Map);
			$MofumUI.goto("game_map");
		});

		//
		var endButton = new BaseButton("退出游戏");
		endButton.setStyleClass("base-button button-5");
		var stop = function() {
			SoundManager.playOk();
			SceneManager.terminate();
		}
		//mixedClick 兼容 触摸屏和点击事件
		endButton.on("mixedClick", function() {
			SoundManager.playOk();
			SceneManager.terminate();
		});

		borderLayout.getSouth().appendChild(startButton);
		borderLayout.getSouth().appendChild(endButton);
	}, "main-menu.css");

	/**
	 * 游戏地图UI
	 * @param {Object} modelName
	 */
	$MofumUI.UI("game_map", function(modelName, cssFile) {
		var frame = new Frame(modelName, cssFile);
		//启用RPGMaker 触摸事件
		frame.setRpgMakerEventCloseable(false);
		var borderLayout = frame.layout;
		var northPanel = borderLayout.getNorth();
		northPanel.setRpgMakerEventCloseable(true);
		northPanel.setHeight(100);
		var southPanel = borderLayout.getSouth();
		southPanel.setHeight(100);

		//聊天面板
		var mqttPanel = new Panel();
		southPanel.appendChild(mqttPanel)
		mqttPanel.setRpgMakerEventCloseable(true);
		mqttPanel.setStyleClass("mqtt-panel a-bounceinT")
		expand = false;
		var expand = false;

		mqttPanel.on("mixedClick", function() {
			if (expand) {
				southPanel.setHeight(100);
				expand = false;
			} else {
				expand = true;
				southPanel.setHeight(300);
			}
			let utterThis = new SpeechSynthesisUtterance();
			utterThis.text = '你好，世界！';
			utterThis.lang = 'zh';//汉语
			utterThis.rate = 0.7;//语速
			speechSynthesis.speak(utterThis);
		});

		var messages = [];
		mqttPanel.on("message", function(e) {
			messages.push(e.detail.message.toString());
		});
		setInterval(function() {
			var content = "";
			for (var counter in messages) {
				//文字内容
				content += "<p class='recive-message'>" + JSON.parse(
						messages[counter])
					.message + "</p>"
			}
			mqttPanel.getElement().scrollTo(mqttPanel.getElement().scrollTopMax);
			if (messages.length > 10) {
				messages.shift();
			}

			mqttPanel.setContent(content)
		})

		setInterval(function() {
			messages.shift();
		}, 5000);

		borderLayout.getCenter().hide();
	}, "game-map.css");

	/**
	 * 游戏 UI
	 * @param {Object} modelName
	 */
	$MofumUI.UI("game_menu", function(modelName, cssFile) {}, "game-menu.css");
})();
