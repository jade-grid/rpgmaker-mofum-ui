/*:
 * @plugindesc UI库 插件
 *
 * @author 玉米
 * @param theme
 * @desc 主题
 * @param autoEnable
 * @desc 自动启动
 */
(function () {

    var parameters = PluginManager.parameters('MofumUI');

    /**
     *定义函数
     * @param callback
     */
    var define = function (callback) {
        callback();
    }

    /**
     * 界面系统
     */
    var _UISystem = {};

    define(function () {

        /**
         * 样式管理器
         */
        function StyleSheetManager() {
            this.initialize.apply(this, arguments);
            ;
        }

        StyleSheetManager.prototype.initialize = function () {
            this.cssFiles = [];
            this.setupSystemCss();
        }

        StyleSheetManager.prototype.setupSystemCss = function () {
            this.cssFiles.push("css/mofumui.css");
            this.cssFiles.push("css/button.css");
            this.cssFiles.push("css/animation.css");
        }

        StyleSheetManager.prototype.loadSystemCss = function () {
            for (var cssIndex in this.cssFiles) {
                this.loadCss(this.cssFiles[cssIndex]);
            }
        }

        StyleSheetManager.prototype.loadCss = function (url, callback) {
            var link = document.createElement('link');
            link.setAttribute("rel", "stylesheet");
            link.setAttribute("type", "text/css");
            link.setAttribute("href", url);
            document.head.appendChild(link);
        }

        StyleSheetManager.prototype.removeAllCustomCss = function () {
            var elements = document.head.querySelectorAll("link[href*='css/custom/']");
            for (var i in elements) {
                if (elements[i] instanceof Element) {
                    elements[i].remove();
                }
            }
        }

        _UISystem.styleSheetManager = new StyleSheetManager();

    });

    /**
     * 定义UI基类
     */
    define(function () {
        /**
         * 界面基类
         * @constructor
         */
        function UIBase() {
            this.initialize.apply(this, arguments);
        }

        /**
         * 静态方法区
         */

        UIBase.setupClass = function (clazz) {
            UIBase.getClasses()[clazz.className] = clazz;
            return clazz;
        }

        UIBase.getClasses = function () {
            if (typeof UIBase.getContext().classes === "undefined" || UIBase.getContext().classes ===
                null) {
                UIBase.getContext().classes = {};
            }
            return UIBase.getContext().classes;
        }

        UIBase.loadClassByName = function (className) {
            return UIBase.getClasses()[className];
        }

        UIBase.initFrameWork = function () {
            var root = UIBase.createRootElement();
            document.body.appendChild(root);
            UIBase.initFrame();
            UIBase.loadAllCss();
            UIBase.destroyId = setInterval(UIBase.refresh, 1);
        }

        UIBase.loadAllCss = function () {
            _UISystem.styleSheetManager.loadSystemCss();
        }

        UIBase.removeAllCustomCss = function () {
            _UISystem.styleSheetManager.removeAllCustomCss();
        }

        UIBase.start = function () {
            UIBase.initFrameWork();
        }

        UIBase.destroy = function () {
            document.body.removeChild(UIBase.getRoot().getElement());
            clearInterval(UIBase.destroyId);
        }

        UIBase.startTimer = function () {
            UIBase.destroyId = setInterval(UIBase.refresh, 1);
        }

        UIBase.stopTimer = function () {
            clearInterval(UIBase.destroyId);
        }

        UIBase.refresh = function () {
            UIBase.refreshRootLayer();
        }
        UIBase.nextId = function () {
            if (typeof UIBase.id === "undefined" || UIBase.id === null) {
                UIBase.id = 0;
                return "mofum_" + UIBase.id;
            }
            UIBase.id++;
            return "mofum_" + UIBase.id;
        }
        /**
         * 静态方法构造元素
         * @param id 元素ID
         * @param tag 元素标签(div,button,input...)
         * @returns {any} 元素本体
         */
        UIBase.createElement = function (tag, id) {
            var element = document.createElement(tag);
            if (typeof id === "undefined" || id === null) {
                id = UIBase.nextId();
            }
            element.setAttribute("id", id);
            UIBase.getContext()[id] = new UIBase();
            UIBase.getContext()[id].setElement(element);
            return element;
        }

        /**
         * 将元素设置为全屏
         * @param element 元素
         */
        UIBase.initFullScreenElement = function (element) {
            if (element) {
                element.style.position = "absolute";
                element.style.left = "0px";
                element.style.right = "0px";
                element.style.top = "0px";
                element.style.bottom = "0px";
                element.style.overflow = "hidden";
            }
        }

        UIBase.loadCss = function (url, callback) {
            _UISystem.styleSheetManager.loadCss(url, callback);
        }

        /**
         * 检测是否缓存过元素
         * @param id 元素ID
         * @returns {boolean} true 有, false 无
         */
        UIBase.exists = function (id) {
            var ui = UIBase.getContext()[id];
            if (!ui) {
                return false;
            }
            var element = ui.getElement();
            if (element) {
                return true;
            } else {
                return false;
            }
        }

        /**
         * 检测元素是否渲染到页面上
         * @param id 元素ID
         * @returns {boolean} true 元素已经渲染到页面上了, false 元素还未渲染
         */
        UIBase.existsDocument = function (id) {
            if (!UIBase.exists(id)) {
                return false;
            }
            var e = document.getElementById(id);
            if (e) {
                return true;
            }
            return false;
        }

        /**
         * 创建根节点
         */
        UIBase.createRootElement = function () {
            var uiRoot = UIBase.createElement("div", UIBase.getRootId());
            UIBase.initFullScreenElement(uiRoot);
            return uiRoot;
        }

        /**
         * 获得根节点ID
         * @returns {string}
         */
        UIBase.getRootId = function () {
            return "UIRoot";
        }

        /**
         * 获取根节点
         * @returns UIBase 根节点
         */
        UIBase.getRoot = function () {
            return this.getContext()[this.getRootId()];
        }

        /**
         * UI全局上下文
         * @returns Object
         */
        UIBase.getContext = function () {
            if (UIBase.context === undefined) {
                UIBase.context = {};
            }
            return UIBase.context;
        }

        UIBase.refreshRootLayer = function () {
            var nodes = document.body.childNodes;
            var maxZIndex = 0;
            for (var nodeIndex = 0; nodeIndex < nodes.length; nodeIndex++) {
                var node = nodes[nodeIndex]
                if (!node.__proto__.getAttribute) {
                    continue;
                }
                var id = node.getAttribute("id");
                if (UIBase.getRootId() == id) {
                    continue;
                }
                maxZIndex = node.style.zIndex > maxZIndex ? node.style.zIndex : maxZIndex;
            }
            if (UIBase.existsDocument(this.getRootId())) {
                UIBase.getRoot().changeLayer(maxZIndex + 100);
            }
        }

        UIBase.putFrame = function (frame) {
            if (frame && frame instanceof UIBase) {
                if (frame.getId()) {
                    return UIBase.frames[frame.getId()] = frame;
                }
            }
        }

        UIBase.getFrame = function (frameId) {
            if (frameId) {
                if (UIBase.frames) {
                    return UIBase.frames[frameId];
                }
            }
        }

        UIBase.initFrame = function () {
            UIBase.frames = {};
        }


        UIBase.currentFrame = function () {
            return UIBase._currentFrame;
        }

        UIBase.goto = function (frameId) {
            if (UIBase._currentFrame) {
                UIBase._currentFrame.hide();
                UIBase.removeAllCustomCss();
            }
            if (frameId) {
                if (UIBase.getFrame(frameId)) {
                    UIBase._currentFrame = UIBase.getFrame(frameId);

                    if (UIBase._currentFrame.getCssFile()) {
                        UIBase.loadCss("css/custom/" + UIBase._currentFrame.getCssFile());
                    }
                    UIBase._currentFrame.show();
                }
            }

        }
        UIBase.contextForEach = function (callback) {
            for (var key in UIBase.getContext()) {
                var ui = UIBase.getContext()[key];
                if (ui instanceof UIBase) {
                    callback(ui);
                }
            }
        }


        /**
         * 实例方法
         */

        UIBase.prototype.initialize = function () {
            this.rpgMakerEventCloseable = true;
            if (this.getElement() && this.rpgMakerEventCloseable) {
                this.closeCanvasEvent();
            }
        }
        UIBase.prototype.on = function (event, callback) {
            var element = this.getElement();
            if (element) {
                element.addEventListener(event, callback);
            }
            if (event === "mixedClick") {
                this.on("click", callback);
                this.on("touchstart", callback);
                this.on("touchmove", callback);
                this.on("touchend", callback);
            }
        }

        UIBase.prototype.setStyleClass = function (styleClass) {
            this.setAttribute("class", styleClass);
        }

        UIBase.prototype.setRpgMakerEventCloseable = function (flag) {
            this.rpgMakerEventCloseable = flag;
        }

        UIBase.prototype.closeEvent = function (e) {
            if (this.rpgMakerEventCloseable) {
                e.preventDefault();
                e.stopPropagation();
            }
        }

        UIBase.prototype.closeCanvasEvent = function () {
            var element = this.getElement();
            if (element) {
                element.addEventListener("click", this.closeEvent.bind(this));
                element.addEventListener("touchstart", this.closeEvent.bind(this));
                element.addEventListener("touchmove", this.closeEvent.bind(this));
                element.addEventListener("touchend", this.closeEvent.bind(this));
                element.addEventListener('mousedown', this.closeEvent.bind(this));
                element.addEventListener('mousemove', this.closeEvent.bind(this));
                element.addEventListener('mouseup', this.closeEvent.bind(this));
                element.addEventListener('wheel', this.closeEvent.bind(this));
                element.addEventListener('touchstart', this.closeEvent.bind(this));
                element.addEventListener('touchmove', this.closeEvent.bind(this));
                element.addEventListener('touchend', this.closeEvent.bind(this));
                element.addEventListener('touchcancel', this.closeEvent.bind(this));
                element.addEventListener('pointerdown', this.closeEvent.bind(this));
            }
        }
        UIBase.prototype.getAttribute = function (attr) {
            return this.getElement().getAttribute(attr);
        };
        UIBase.prototype.setAttribute = function (attr, value) {
            return this.getElement().setAttribute(attr, value);
        };

        UIBase.prototype.setId = function (id) {
            this.setAttribute("id", id);
        };
        UIBase.prototype.getStyle = function () {
            if (!this.checkElementExist()) {
                return {};
            }
            return this.getElement().style;
        }

        UIBase.prototype.setElement = function (element) {
            this.element = element;
        }

        UIBase.prototype.getElement = function () {
            return this.element;
        }

        UIBase.prototype.checkElementExist = function () {
            if (this.element) {
                return true;
            }
            return false;
        }

        UIBase.prototype.show = function () {
            this.getStyle().display = "block";
        }

        UIBase.prototype.hide = function () {
            this.getStyle().display = "none";
        }

        UIBase.prototype.setContent = function (content) {
            this.getElement().innerHTML = content;
        }

        UIBase.prototype.setFullWidth = function () {
            this.getStyle().width = "100%";
        }

        UIBase.prototype.setFullHeight = function () {
            this.getStyle().height = "100%";
        }

        UIBase.prototype.setWidth = function (width) {
            this.getStyle().width = width + "px";
        }

        UIBase.prototype.setHeight = function (height) {
            this.getStyle().height = height + "px";
        }

        UIBase.prototype.getHeight = function () {
            return this.getElement().offsetHeight;
        }

        UIBase.prototype.getWidth = function () {
            return this.getElement().offsetWidth;
        }

        UIBase.prototype.setFontColor = function (color) {
            this.getStyle().color = color;
        }

        UIBase.prototype.setFontSize = function (size) {
            this.getStyle().fontSize = size;
        }

        UIBase.prototype.setLineHeight = function (height) {
            this.getStyle().lineHeight = height;
        }

        UIBase.prototype.setTextAlign = function (align) {
            this.getStyle().textAlign = align;
        }

        UIBase.prototype.setBackgroundColor = function (color) {
            this.getStyle().backgroundColor = color;
        }

        UIBase.prototype.setPositionMethod = function (positionMethod) {
            this.getStyle().position = positionMethod;
        }

        UIBase.prototype.setPositionRelative = function () {
            this.setPositionMethod("relative")
        }

        UIBase.prototype.setPositionAbsolute = function () {
            this.setPositionMethod("absolute")
        }

        UIBase.prototype.setLocation = function (left, top, right, bottom) {
            this.setLeft(left);
            this.setTop(top);
            this.setRight(right);
            this.setBottom(bottom);
        }

        UIBase.prototype.setLeft = function (num) {
            this.getStyle().left = num + "px";
        }
        UIBase.prototype.setTop = function (num) {
            this.getStyle().top = num + "px";
        }
        UIBase.prototype.setRight = function (num) {
            this.getStyle().right = num + "px";
        }
        UIBase.prototype.setBottom = function (num) {
            this.getStyle().bottom = num + "px";
        }

        /**
         * 更换显示层级
         * @param layer 层
         */
        UIBase.prototype.changeLayer = function (layer) {
            if (!this.checkElementExist()) {
                return;
            }
            this.getElement().style.zIndex = layer;
        }
        UIBase.prototype.getChildNodes = function () {
            if (this.childNodes) {
                return this.childNodes;
            }
            this.childNodes = [];
            return this.childNodes;
        }

        UIBase.prototype.getId = function () {
            return this.getElement().getAttribute("id");
        }

        UIBase.prototype.setBorder = function (border) {
            this.getStyle().border = border
        }

        UIBase.prototype.setBorderRadius = function (borderRadius) {
            this.getStyle().borderRadius = borderRadius
        }

        UIBase.prototype.appendChild = function (ui) {
            this.getElement().appendChild(ui.getElement());
            this.getChildNodes().push(ui);
        }

        UIBase.prototype.appendChildNodes = function (array) {
            if (Array.isArray(array)) {
                for (var counter = 0; counter > array.length() - 1; counter++) {
                    this.appendChild(array[counter]);
                }
            }
        }
        UIBase.prototype.setDisplay = function (display) {
            this.getStyle().display = display;
        }

        window.$MofumUI = UIBase;
        /**
         * window 相关配置配置
         */
        (function () {
            window.addEventListener("mqttMessage", function (e) {
                UIBase.contextForEach(function (ui) {
                    var uimessage = new CustomEvent('message', {
                        "detail": e.detail
                    })
                    if (ui.getElement().dispatchEvent) {
                        ui.getElement().dispatchEvent(uimessage);
                        return;
                    }

                    if (ui.getElement().fireEvent) {
                        ui.getElement().fireEvent(uimessage);
                        return;
                    }
                });
            })

            window.onresize = function () {
                UIBase.contextForEach(function (ui) {
                    var resize = new CustomEvent('windowResize', {
                        "detail": {
                            width: window.innerWidth,
                            height: window.innerHeight
                        }
                    })
                    if (ui.getElement().dispatchEvent) {
                        ui.getElement().dispatchEvent(resize);
                        return;
                    }

                    if (ui.getElement().fireEvent) {
                        ui.getElement().fireEvent(resize);
                        return;
                    }
                });

            }
        })();
    });

    $MofumUI.Utils = {};

    define(function () {

        /**
         * 定时器
         * @constructor
         */
        function Timer() {
            this.initialize.apply(this, arguments);
        }

        Timer.prototype.initialize = function () {
            this.clearTimeoutId = 0;
            this.sleepTime = 1000;
            this.state = "NEW";
        }

        Timer.prototype.setSleepTime = function (sleepTime) {
            this.sleepTime = sleepTime;
        }
        Timer.prototype.start = function () {
            this.clearTimeoutId = setInterval(this._run.bind(this), this.sleepTime);
        }

        Timer.prototype._run = function () {
            this.state = "RUNNING";
            if (this.run) {
                this.run().bind(this);
            }
            this.state = "DEAD";
        }

        Timer.prototype.run = function () {

        }

        Timer.prototype.stop = function () {
            if (this.state) {
                if (this.state === "DEAD") {
                    clearInterval(this.clearTimeoutId);
                }
            }
        }

        Timer.prototype.getState = function () {
            return this.state;
        }


        $MofumUI.Utils.Timer = Timer;

        /**
         * 定时器池
         * @constructor
         */
        function TimerPool() {
            this.initialize.apply(this, arguments);
        }

        TimerPool.prototype.initialize = function () {
            this.pools = [];
            this.taskList = [];
        }

        TimerPool.prototype.getSize = function () {
            return this.pools.length;
        }

        TimerPool.prototype.push = function (task) {
            this.taskList.push(task);
        }

        TimerPool.prototype.pop = function () {
            return this.taskList.shift();
        }

        TimerPool.prototype.submit = function (task) {
            this.taskList.push(task);
        }

        $MofumUI.Utils.TimerPool = TimerPool;

    });

    define(function () {

    });


    /**
     * 定义类
     * @param name 名称
     * @param callback 构造回调
     */
    var defineClass = function (name, root, callback) {
        var rootClazz = null;
        if (typeof root === "undefined" || root === null) {
            rootClazz = window.$MofumUI;
        } else {
            rootClazz = window.$MofumUI.loadClassByName(root);
        }
        var model = callback(rootClazz);
        model.className = name;
        window.$MofumUI.setupClass(model);
    }

    function UI(modelName, callback, cssFile) {
        callback(modelName, cssFile);
    }

    $MofumUI.UI = UI;
    $MofumUI.define = define;
    $MofumUI.defineClass = defineClass;

    /**
     * 面板
     */
    defineClass("Panel", null, function (rootClazz) {

        function Panel() {
            this.initialize.apply(this, arguments);
        }

        Panel.prototype = Object.create(rootClazz.prototype);
        Panel.prototype.constructor = Panel;

        Panel.prototype.initialize = function () {
            this.element = $MofumUI.createElement("div");
            this.closeCanvasEvent();
        };

        return Panel;

    });


    /**
     * 基础布局类
     */
    defineClass("BaseLayout", null, function (rootClazz) {

        function BaseLayout() {
            this.initialize.apply(this, arguments);
            this.closeCanvasEvent();
        }

        BaseLayout.prototype = Object.create(rootClazz.prototype);
        BaseLayout.prototype.constructor = BaseLayout;

        BaseLayout.prototype.initialize = function () {
            this.element = $MofumUI.createElement("div");
            this.element.setAttribute("layout-type", "base");
        };

        return BaseLayout;

    });

    /**
     * 基础按钮
     */
    defineClass("BaseButton", null, function (rootClazz) {

        function BaseButton() {
            this.initialize.apply(this, arguments);
            this.closeCanvasEvent();
        }

        BaseButton.prototype = Object.create(rootClazz.prototype);
        BaseButton.prototype.constructor = BaseButton;

        BaseButton.prototype.initialize = function () {
            this.element = $MofumUI.createElement("button");
            this.element.setAttribute("button-type", "BaseButton");
            this.setPositionRelative();
            if (arguments[0]) {
                this.setContent(arguments[0]);
            }
        };

        return BaseButton;

    });

    /**
     * Div按钮
     */
    defineClass("DivButton", "BaseButton", function (rootClazz) {

        function DivButton() {
            this.initialize.apply(this, arguments);
            this.closeCanvasEvent();
        }

        DivButton.prototype = Object.create(rootClazz.prototype);
        DivButton.prototype.constructor = DivButton;

        DivButton.prototype.initialize = function () {
            this.element = $MofumUI.createElement("div");
            this.element.setAttribute("button-type", "DivButton");
            this.setPositionRelative();
            if (arguments[0]) {
                this.setContent(arguments[0]);
            }
        };

        return DivButton;

    });

    /**
     * 基础图片
     */
    defineClass("BaseImage", null, function (rootClazz) {

        function BaseImage() {
            this.initialize.apply(this, arguments);
        }

        BaseImage.prototype = Object.create(rootClazz.prototype);
        BaseImage.prototype.constructor = BaseImage;

        BaseImage.prototype.initialize = function () {
            this.element = $MofumUI.createElement("img");
            this.element.setAttribute("img-type", "img");
            if (arguments[0]) {
                this.setSrc(arguments[0]);
            }
        };

        BaseImage.prototype.setSrc = function (src) {
            this.element.setAttribute("src", src);
        }

        return BaseImage;

    });

    /**
     * UI 框架
     */
    defineClass("Frame", null, function (rootClazz) {

        function Frame() {
            this.initialize.apply(this, arguments);
        }

        Frame.prototype = Object.create(rootClazz.prototype);
        Frame.prototype.constructor = Frame;

        Frame.prototype.initialize = function () {
            this.element = $MofumUI.createElement("div");
            $MofumUI.getRoot().appendChild(this);
            var BorderLayout = $MofumUI.loadClassByName("BorderLayout");
            this.layout = new BorderLayout(this);
            this.setFullHeight();
            this.setFullWidth();
            this.setPositionRelative();
            if (arguments[0]) {
                this.setId(arguments[0]);
            }
            if (arguments[1]) {
                this.setCssFile(arguments[1]);
            }
            $MofumUI.putFrame(this);
            this.hide();
        };

        Frame.prototype.setLayout = function (layout) {
            this.layout = layout;
        }
        Frame.prototype.setCssFile = function (cssFile) {
            this.cssFile = cssFile;
        };

        Frame.prototype.show = function (cssFile) {
            this.refresh();
        };

        Frame.prototype.getCssFile = function () {
            return this.cssFile;
        }

        Frame.prototype.refresh = function () {
            if (this.layout) {
                // this.layout.refresh();
            }
        }

        return Frame;

    });

    /**
     * 重写RPGMaker原生场景
     *
     *
     */
    defineClass("SceneFrame", "Scene_Base", function (rootClazz) {
        function SceneFrame() {
            this.initialize.apply(this, arguments);
        }

        var Frame = $MofumUI.loadClassByName("Frame");

        SceneFrame.prototype = Object.create(rootClazz.prototype);
        SceneFrame.prototype.constructor = SceneFrame;

        SceneFrame.prototype.initialize = function () {
            if (arguments[0] instanceof Frame) {
                this.wrapperFrame = arguments[0];
            }
        };
        return SceneFrame;
    })


    /**
     * 边框布局，东西南北中
     * 该布局为动态布局，部分css会失效，比如高度，宽度
     */
    defineClass("BorderLayout", "BaseLayout", function (rootClazz) {
        function BorderLayout() {
            this.initialize.apply(this, arguments);
        }

        BorderLayout.prototype = Object.create(rootClazz.prototype);
        BorderLayout.prototype.constructor = BorderLayout;

        BorderLayout.prototype.initialize = function () {
            this.element = $MofumUI.createElement("div");
            this.element.setAttribute("layout-type", "border");
            this.setFullHeight();
            this.setFullWidth();
            this.setPositionRelative();
            if (arguments[0] instanceof $MofumUI) {
                arguments[0].appendChild(this);
            } else {
                $MofumUI.getRoot().appendChild(this);
            }
            this.initPanel();
            this.refresh();
            this.closeCanvasEvent();
        };

        BorderLayout.prototype.refresh = function () {
            this.initNorth();
            this.initSouth();
            this.initEast();
            this.initWest();
            this.initCenter();
            this.initBackgroud();
        }

        BorderLayout.prototype.initPanel = function () {
            this.panels = {};
            this.panels["south"] = this.createPanel("south");
            this.panels["north"] = this.createPanel("north");
            this.panels["east"] = this.createPanel("east");
            this.panels["west"] = this.createPanel("west");
            this.panels["center"] = this.createPanel("center");
            this.panels["background"] = this.createPanel("background");
        }

        BorderLayout.prototype.initBackgroud = function () {
            this.getBackground().changeLayer(1);
        };

        BorderLayout.prototype.initEast = function () {
            this.getEast().setFullHeight();
            this.getEast().setLocation(null, this.getNorth().getHeight(), 0, this.getSouth()
                .getHeight());
        };

        BorderLayout.prototype.initWest = function () {
            this.getWest().setFullHeight();
            this.getWest().setLocation(0, this.getNorth().getHeight(), null, this.getSouth()
                .getHeight());
        };

        BorderLayout.prototype.initSouth = function () {
            this.getSouth().setLocation(0, null, 0, 0);
            this.getSouth().setFullWidth();
        };
        BorderLayout.prototype.initNorth = function () {
            this.getNorth().setLocation(0, 0, 0, null);
            this.getNorth().setFullWidth();
        };
        BorderLayout.prototype.initCenter = function () {
            this.getCenter().setLocation(
                this.getWest().getWidth(),
                this.getNorth().getHeight(),
                this.getEast().getWidth(),
                this.getSouth().getHeight()
            );
        };

        BorderLayout.prototype.getEast = function () {
            return this.getPanel("east");
        };

        BorderLayout.prototype.getWest = function () {
            return this.getPanel("west");
        };

        BorderLayout.prototype.getSouth = function () {
            return this.getPanel("south");
        };

        BorderLayout.prototype.getNorth = function () {
            return this.getPanel("north");
        };

        BorderLayout.prototype.getCenter = function () {
            return this.getPanel("center");
        };

        BorderLayout.prototype.getBackground = function () {
            return this.getPanel("background");
        };

        BorderLayout.prototype.getPanel = function (position) {
            return this.getPanels()[position];
        };


        BorderLayout.prototype.setHeight = function () {
            rootClazz.prototype.setHeight.call(this);
            this.refresh();
        }


        BorderLayout.prototype.setWidth = function () {
            rootClazz.prototype.setWidth.call(this);
            this.refresh();
        }
        /**
         * 获取面板
         * East, West, South, North,Center
         */
        BorderLayout.prototype.getPanels = function () {
            return this.panels;
        };

        BorderLayout.prototype.createPanel = function (position) {
            var Panel = $MofumUI.loadClassByName("Panel");
            var panel = new Panel();
            panel.getElement().setAttribute("border-layout-type", position);
            panel.setPositionAbsolute();
            panel.setWidth(0);
            panel.setHeight(0);
            panel.changeLayer(3);
            panel.setStyleClass("layout-" + position)
            this.appendChild(panel);
            return panel;
        }
        return BorderLayout;
    });

    /**
     * Live2D面板
     */
    defineClass("Live2D", "Panel", function (rootClazz) {

        function Live2D() {
            this.initialize.apply(this, arguments);
            this.closeCanvasEvent();
        }

        Live2D.prototype = Object.create(rootClazz.prototype);
        Live2D.prototype.constructor = Live2D;

        Live2D.prototype.initialize = function () {
            this.element = $MofumUI.createElement("div");
            this.element.setAttribute("live-type", "Live2D");
            this.setPositionRelative();
            if (arguments[0]) {
                this.setContent(arguments[0]);
            }
        };

        return Live2D;

    });

    /**
     * 轮播面板
     */
    defineClass("Carousel", "Panel", function (rootClazz) {

        function Carousel() {
            this.initialize.apply(this, arguments);
            this.closeCanvasEvent();
        }

        Carousel.prototype = Object.create(rootClazz.prototype);
        Carousel.prototype.constructor = Carousel;

        Carousel.prototype.initialize = function () {
            this.element = $MofumUI.createElement("div");
            this.element.setAttribute("carousel-type", "Carousel");
            this.setPositionRelative();
            if (arguments[0]) {
                this.setContent(arguments[0]);
            }
        };

        return Carousel;

    });

    /**
     * 弹幕面板
     */
    defineClass("Barrage", "Panel", function (rootClazz) {

        function Barrage() {
            this.initialize.apply(this, arguments);
            this.closeCanvasEvent();
        }

        Barrage.prototype = Object.create(rootClazz.prototype);
        Barrage.prototype.constructor = Barrage;

        Barrage.prototype.initialize = function () {
            this.element = $MofumUI.createElement("div");
            this.element.setAttribute("barrage-type", "Barrage");
            this.setPositionRelative();
            if (arguments[0]) {
                this.setContent(arguments[0]);
            }
        };
        return Barrage;
    });


}());
